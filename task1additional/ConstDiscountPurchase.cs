﻿namespace task1additional
{
    class ConstDiscountPurchase : Purchase
    {
        private readonly int NeededQuantity;
        private int Discount { get; set; }

        public ConstDiscountPurchase(string name, double price, int quantity, int discount, int neededQuantity) : base(name, price, quantity)
        {
            NeededQuantity = neededQuantity;
            Discount = discount;
        }

        public override double GetCost()
        {
            if (Quantity >= NeededQuantity)
                return (base.GetCost() * (1 - Discount / 100));
            else
                return (base.GetCost());
        }

        public override string ToString()
        {
            return (base.ToString() + ";" + Discount.ToString("0.000"));
        }
    }
}

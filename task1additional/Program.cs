﻿using System;

namespace task1additional
{
    class Program
    {
        static void Main()
        {
            bool productsEqual = true;

            Purchase[] mas =
                {
                new Purchase("Pipe", 3.50, 3),
                new Purchase("Hammer", 9.15, 1),
                new DiscountPurchase("Axe", 12.30, 2, 2.10),
                new DiscountPurchase("Nails", 0.50, 30, 0.10),
                new ConstDiscountPurchase("Saw", 10.90, 1, 50, 3),
                new ConstDiscountPurchase("Shovel", 9.00, 3, 25, 2),
            };

            Purchase maxP = mas[0];

            for (int i = 0; i < mas.Length; i++)
            {
                Console.WriteLine(mas[i]);
                if (mas[i].GetCost() > maxP.GetCost())
                {
                    maxP = mas[i];
                }
                if ((i + 1 != mas.Length) && productsEqual)
                {
                    if (!(mas[i].Equals(mas[i + 1])))
                        productsEqual = false;
                }
            }

            Console.WriteLine("Product with a maximum value - " + maxP.Name);
            if (productsEqual)
                Console.WriteLine("All products are equal");
            else
                Console.WriteLine("Products aren't equal");

            Console.ReadKey();
        }
    }
}

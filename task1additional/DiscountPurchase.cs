﻿namespace task1additional
{
    class DiscountPurchase : Purchase
    {
        public double Discount { get; set; }

        public DiscountPurchase(string name, double price, int quantity, double discount) : base(name, price, quantity)
        {
            Discount = discount;
        }

        public override double GetCost()
        {
            return ((Price - Discount) * Quantity);
        }

        public override string ToString()
        {
            return (base.ToString() + ";" + Discount.ToString("0.00"));
        }
    }
}
